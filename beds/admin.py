from django.contrib import admin
from django.contrib.admin import ModelAdmin

from beds.models import Bed


@admin.register(Bed)
class EnergyLogAdmin(ModelAdmin):
    list_display = ['bed', 'created_at']
    search_fields = ['bed']
    list_filter = ('bed', 'created_at')