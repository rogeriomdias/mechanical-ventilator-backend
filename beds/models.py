from django.db.models import CharField
from django.utils.translation import ugettext_lazy as _

from utils.base_model import BaseModel


class Bed(BaseModel):
    bed = CharField(verbose_name=_("Bed"), max_length=50, default='')

    class Meta:
        verbose_name = _('Bed')
        verbose_name_plural = _('Beds')
