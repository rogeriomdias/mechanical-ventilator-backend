from typing import Optional

from django.contrib.auth import get_user_model
from django.test import TestCase
from django.urls import resolve, reverse
from rest_framework import status
from rest_framework.test import APITestCase
from rest_framework_simplejwt.tokens import RefreshToken

User = get_user_model()


class TestURLsBaseClass(TestCase):
    def setUp(self):
        self.list_path = []

    def test_reverse(self):
        for (rev, url) in self.list_path:
            if len(rev) == 2:
                self.assertEqual(reverse(rev[0], kwargs=rev[1]), url)
            else:
                self.assertEqual(reverse(rev), url)

    def test_resolve(self):
        for (view_name, res) in self.list_path:
            if len(view_name) == 2:
                self.assertEqual(resolve(res).view_name, view_name[0])
            else:
                self.assertEqual(resolve(res).view_name, view_name)


class BaseViewTestCase(APITestCase):
    def setUp(self):
        self.admin_username = 'admin@test.com'
        self.username = 'user@test.com'
        self.password = 'testuserpass'
        self.cpf = '33939815012'

        self.admin = User.objects.create_user(username=self.admin_username, password=self.password, email=self.username,
                                              first_name="Admin", last_name="Test", is_staff=True, is_superuser=True,
                                              is_email_verified=True)

        self.user = User.objects.create_user(username=self.username, password=self.password, email=self.username,
                                             first_name="User", last_name="Test", is_email_verified=False)

        self.token = RefreshToken.for_user(self.user)
        self.access_key = self.token.access_token
        self.admin_token = RefreshToken.for_user(self.admin)
        self.admin_access_key = self.admin_token.access_token

    @staticmethod
    def auth_params(access_key):
        return {'HTTP_AUTHORIZATION': "Bearer {}".format(access_key)} if access_key else {}

    def api_get(self, path: str, access_key: str = None, path_kwargs=None, req_format: Optional[str] = "json",
                get_param: str = ""):
        return self.client.get(
            reverse(path, kwargs=path_kwargs) + get_param, format=req_format, **self.auth_params(access_key)
        )

    def api_post(self, path: str, data, access_key: str = None, path_kwargs=None, req_format: Optional[str] = "json"):
        return self.client.post(
            reverse(path, kwargs=path_kwargs), data=data, format=req_format, **self.auth_params(access_key)
        )

    def api_put(self, path: str, data, access_key: str = None, path_kwargs=None, req_format: Optional[str] = "json"):
        return self.client.put(
            reverse(path, kwargs=path_kwargs), data=data, format=req_format, **self.auth_params(access_key)
        )

    def api_patch(self, path: str, data, access_key: str = None, path_kwargs=None, req_format: Optional[str] = "json"):
        return self.client.patch(
            reverse(path, kwargs=path_kwargs), data=data, format=req_format, **self.auth_params(access_key)
        )

    def api_delete(self, path: str, access_key: str = None, path_kwargs=None, req_format: Optional[str] = "json"):
        return self.client.delete(
            reverse(path, kwargs=path_kwargs), format=req_format, **self.auth_params(access_key)
        )

    def assertResponse200(self, response):
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def assertResponse201(self, response):
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def assertResponse400(self, response):
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def assertResponse302(self, response):
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
