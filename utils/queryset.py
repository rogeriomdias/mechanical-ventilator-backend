from django.db.models import QuerySet


class BaseQuerySet(QuerySet):
    def pk(self, pk):
        return self.get(pk=pk)

    def get_or_none(self, *args, **kwargs):
        try:
            return super().get(*args, **kwargs)
        except self.model.DoesNotExist:
            return None


class BaseModelQuerySet(QuerySet):
    def created_today(self, *args, **kwargs):
        from datetime import date
        today = date.today()
        return self.filter(created_at__year=today.year, created_at__month=today.month, created_at__day=today.day)

    def occurred_today(self, *args, **kwargs):
        from datetime import date
        today = date.today()
        return self.filter(occurred_at__year=today.year, occurred_at__month=today.month, occurred_at__day=today.day)
