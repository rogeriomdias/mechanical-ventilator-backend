from datetime import datetime

from django.db.models import CASCADE, ForeignKey, FloatField
from django.utils.translation import ugettext_lazy as _

from utils.base_model import BaseModel
from utils.queryset import BaseModelQuerySet


class VitalLogQuerySet(BaseModelQuerySet):
    def sensor(self, sensor):
        return self.filter(sensor=sensor)

    def started_at(self, start_date: datetime):
        return self.filter(created_at__gte=start_date)

    def finished_at(self, end_date: datetime):
        return self.filter(created_at__lte=end_date)

    def occurred_at_range(self, start_date: datetime, end_date: datetime):
        return self.started_at(start_date).finished_at(end_date)

    def latest_or_none(self, field_name):
        if not self.exists():
            return VitalLog.objects.none()
        else:
            return [self.latest(field_name)]


class VitalLog(BaseModel):
    sensor = ForeignKey('sensors.Sensor', on_delete=CASCADE, verbose_name=_('Sensor'))
    measured_value = FloatField(verbose_name=_('Measured Value'))

    class Meta:
        verbose_name = _('Vital Log')
        verbose_name_plural = _('Vital Logs')

    def __str__(self):
        return f'{self.sensor.name} - {self.created_at}'
