from django.contrib import admin
from django.contrib.admin import ModelAdmin

from vital_log.models import VitalLog


@admin.register(VitalLog)
class EnergyLogAdmin(ModelAdmin):
    list_display = ['sensor', 'measured_value', 'created_at']
    search_fields = ['sensor']
    list_filter = ('sensor', 'created_at')
