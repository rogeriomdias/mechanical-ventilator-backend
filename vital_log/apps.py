from django.apps import AppConfig


class VitalLogConfig(AppConfig):
    name = 'vital_log'
