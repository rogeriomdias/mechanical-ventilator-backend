from django.contrib import admin
from django.contrib.admin import ModelAdmin

from sensors.models import Sensor


@admin.register(Sensor)
class EnergyLogAdmin(ModelAdmin):
    list_display = ['bed', 'sensor_name', 'sensor_type', 'created_at']
    search_fields = ['bed', 'sensor_type']
    list_filter = ('bed', 'created_at')