from django.utils.translation import ugettext_lazy as _

NOT_ASSIGNED = 0
HEARTBEAT = 1
TEMPERATURE = 2
RESPIRATOR = 3

SENSOR_CHOICES = [(NOT_ASSIGNED, _("Not Assigned")), (HEARTBEAT, _("Heartbeat")), (TEMPERATURE, _("Temperature")),
                  (RESPIRATOR, _("Respirator"))]
