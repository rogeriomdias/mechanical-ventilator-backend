from django.db.models import CASCADE, CharField, ForeignKey, IntegerField
from django.utils.translation import ugettext_lazy as _

from sensors.sensor_utils import SENSOR_CHOICES
from utils.base_model import BaseModel


class Sensor(BaseModel):
    bed = ForeignKey('beds.Bed', on_delete=CASCADE, verbose_name=_('Bed'))
    sensor_type = IntegerField(choices=SENSOR_CHOICES)
    sensor_name = CharField(max_length=50, verbose_name=_('Sensor Name'), null=True, blank=True)

    class Meta:
        verbose_name = _('Sensor')
        verbose_name_plural = _('Sensors')
